const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product");
const index = require("../index");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access : auth.createAccessToken(result)};
			}
			else{
				return false;
			}
		}
	})
}

module.exports.addToCart = async(reqBody, isAdmin, userId) => {
	console.log("Inside add to cart");
	if(!isAdmin){
		let changesInCart = {
			userId : userId,
			cartChanges : [{
				productId: reqBody.productId,
				quantity: 1
			}]
		}
		console.log(changesInCart);

		let len = 1;
		let i = 0;
		let productDetails = [];
		let previousData = [];
		let newOrContinue = "";
		let count = 0;

		let firstoOrNot = await User.findById(changesInCart.userId).then(user => {
			return user.orders.length
		})

		if(firstoOrNot !== 0){
			newOrContinue = await User.findById(changesInCart.userId).then(user => {
				if (user.orders[user.orders.length - 1].isCheckedOut === false){
					return "old";
				}
				else{
					return "new";
				}
			})
		}
		
		while (i < len) {
			productDetails.push(await Product.findById(changesInCart.cartChanges[i].productId).then(result => {
					return result;
				}))
			i += 1;
		};

		let isUserUpdated = await User.findById(changesInCart.userId).then(user => {
			let i = 0;
			let dummyPreviousData = {
				productName : "",
				quantity : 0,
				subTotal : 0
			}

			if(newOrContinue === "old"){
				i = 0;
				while (i < user.orders[user.orders.length - 1].product.length){
					previousData.push(user.orders[user.orders.length - 1].product[i]);
					i += 1
				}
				user.orders.pop();
			}
			else{
				i = 0;
				while(i < len){
					previousData.push(dummyPreviousData);
					i += 1;
				}
			}

			let j = 0;
			let subTotal = 0;
			let amount = 0;
			let subList = [];
			let tempCartList = [];

			while(j < changesInCart.cartChanges.length){
				tempCartList.push(changesInCart.cartChanges[j])
				j += 1
			}

			let found = false;

			i = 0;
			while(i < productDetails.length){
				j = 0;
				while(j < previousData.length){
					if(productDetails[i].name === previousData[j].productName){
						subTotal = (previousData[j].quantity + tempCartList[i].quantity) * productDetails[i].price;
						amount += (previousData[j].quantity + tempCartList[i].quantity) * productDetails[i].price;
						subList.push({productName: productDetails[i].name, quantity: previousData[j].quantity + tempCartList[i].quantity,
								subTotal: subTotal});
						productDetails.splice(i, 1);
						tempCartList.splice(i, 1);
						previousData.splice(j, 1);
						found = true
						if(productDetails.length === 0){
							break;
						}
					}
					if(found){
						j += 0;
						found = false;
					}
					else{
						j += 1;
					}
				}
				if(productDetails.length === 0){
					break;
				}
				else if(found){
					i += 0;
					found = false;
				}
				else{
					i += 1;
				}
			}

			i = 0;
			while(i < productDetails.length){
				subTotal = tempCartList[i].quantity * productDetails[i].price;
				amount += tempCartList[i].quantity * productDetails[i].price;
				subList.push({productName: productDetails[i].name, quantity: tempCartList[i].quantity,
					subTotal: subTotal});
				i += 1
			}

			j = 0;
			while(j < previousData.length){
				if(previousData[j].productName !== ""){
					subTotal = previousData[j].subTotal;
					amount += previousData[j].subTotal;
					subList.push({productName: previousData[j].productName, quantity: previousData[j].quantity, subTotal: subTotal});
				}				 
				j += 1
			}

			user.orders.push({product: subList, totalAmount: amount});

			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		i = 0;
		let isProductUpdated = false;
		let orderId = await User.findById(changesInCart.userId).then(user => {
			let newLen = user.orders.length;
			return user.orders[newLen - 1]._id;
		})
		while(i < len){
			isProductUpdated = await Product.findById(changesInCart.cartChanges[i].productId).then(product => {
				product.quantity -= changesInCart.cartChanges[i].quantity;
				product.orders.push({orderId : orderId});

				return product.save().then((product, error) => {
					if(error) {
						return false;
					}
					else{
						return true;
					}
				})
			})
			i += 1;
		}

		if(isUserUpdated && isProductUpdated){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return false;
	}
} 

module.exports.checkOut = async(data) => {
	if(!data.isAdmin){

		return  await User.findById(data.userId).then(user => {
			if(user.orders[user.orders.length - 1].isCheckedOut === false){
				user.orders[user.orders.length - 1].isCheckedOut = true;

				return user.save().then((user, error) => {
					if(error){
						return false;
					}
					else{
						return true;
					}
				})
			}	
		})
	}
	else{
			return false;
		}
}

module.exports.Cart = async(isAdmin, userId) => {
	if(!isAdmin){

		return await User.findById(userId).then(user => {
			if(user.orders[user.orders.length - 1].isCheckedOut === false){
				return user.orders[user.orders.length - 1];
			}
		})
	}
}

module.exports.getUserProfile = (userId) => {
	return User.findById(userId).then(result => {
		if(result === null){
			return false;
		}
		else{
			result.password = "";
			return result;
		}
	})
}

module.exports.getAllUsers = (isAdmin) => {
	if(isAdmin){
		return User.find({}).then(result => {
			return result;
		})
	}
	else{
		return false;
	}
	
}


module.exports.setUserAsAdmin = (isAdmin, reqParams, reqBody) => {
	if(isAdmin){
		let setAsAdmin = {
			isAdmin : reqBody.isAdmin
		}
		return User.findByIdAndUpdate(reqParams.userId, setAsAdmin).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}

module.exports.getUserOrderDetails = (userId, isAdmin) => {
	return User.findById(userId).then((user, error) => {
		if(error){
			return false;
		}
		else{
			return user.orders;
		}
	})
}

module.exports.getAllOrders = (isAdmin) => {
	if(isAdmin){
		return User.find({}).then((result, error) => {
			if(error){
				return false;
			}
			else{
				let userCount = result.length;
				let i = 0;
				allOrders = [];
				while (i < userCount){
					let userOrderLength = result[i].orders.length;
					let j = 0;
					while (j < userOrderLength){
						allOrders.push(result[i].orders[j]);
						j += 1;
					}
					i += 1;
				}
				return allOrders;
			}
		})
	}
	else{
		return false;
	}	
}