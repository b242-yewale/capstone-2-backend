const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	if (data.isAdmin){
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			quantity : data.product.quantity
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}

module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})	
}

module.exports.getAllActiveProducts = (isAdmin) => {
	if(isAdmin){
		return Product.find({isActive : true}).then(result => {
			return result;
		})
	}
	else{
		return false;
	}
}

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

module.exports.updateProduct = (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
		let updatedProduct = {
				name : reqBody.name,
				description : reqBody.description,
				price : reqBody.price,
				quantity : reqBody.quantity
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}	
}

module.exports.archiveProduct = (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
		let updateIsActive = {
			isActive : reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, updateIsActive).then((product, error) => {

			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
	else{
		return false;
	}
}

module.exports.updateProductQuantity = async (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
		let updatedQuantityProduct = await Product.findById(reqParams.productId).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return product;
			}			
		})
		console.log(updatedQuantityProduct)
		let updateQuantity = {
			quantity : updatedQuantityProduct.quantity + reqBody.newStock
		}

		return await Product.findByIdAndUpdate(reqParams.productId, updateQuantity).then((product, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	}
}
