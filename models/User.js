const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "email is required"]
	},
	password : {
		type : String,
		required : [true, "password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
		{
			product : [
				{
					productName : {
							type : String,
							required : [true, "productName is required"]
					},
					quantity : {
							type : Number,
							required : [true, "quantity is required"]
					},
					subTotal : {
							type : Number,
							required : [true, "subTotal is required"]
					}
				}
			],
			isCheckedOut : {
				type : Boolean,
				default : false
			},
			totalAmount : {
				type : Number,
				required : [true, "totalAmount is required"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}	
	]
});

module.exports = mongoose.model("User", userSchema);