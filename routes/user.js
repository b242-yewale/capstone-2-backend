const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/addToCart", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id
	userController.addToCart(req.body, isAdmin, userId).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id
	}

	userController.checkOut(data).then(resultFromController => res.send(resultFromController));
});

router.get("/cart", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id
	userController.Cart(isAdmin, userId).then(resultFromController => res.send(resultFromController));
});

router.get("/userDetails", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	userController.getUserProfile(userId).then(resultFromController => res.send(resultFromController));
});

router.get("/allUsers", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllUsers(isAdmin).then(resultFromController => res.send(resultFromController));
})

router.post("/setAsAdmin/:userId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.setUserAsAdmin(isAdmin, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/getUserOrders",auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization).id;
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getUserOrderDetails(userId, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get("/getAllOrders", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	userController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});





module.exports = router;