const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin 
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
});

router.get("/", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.getAllActiveProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	console.log("here")

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId", auth.verify, (req, res) => {
	isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.put("/:productId/archive", auth.verify, (req, res) => {
	isAdmin = auth.decode(req.headers.authorization).isAdmin
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.put("/updateQuantity/:productId", auth.verify, (req, res) => {
	isAdmin = auth.decode(req.headers.authorization).isAdmin;
	productController.updateProductQuantity(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;